package org.marsrover;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RoverTest {

	@Test
	public void turnLeftN()
	{
		Rover r = new Rover(1, 2, Direction.N);
		r.turnLeft();
		assertEquals(r.getD(), Direction.W);
		assertEquals(r.getCurrentX(), 1);
		assertEquals(r.getCurrentY(), 2);
	}
	@Test
	public void turnLeftE()
	{
		Rover r = new Rover(1, 2, Direction.E);
		r.turnLeft();
		assertEquals(r.getD(), Direction.N);
		assertEquals(r.getCurrentX(), 1);
		assertEquals(r.getCurrentY(), 2);
	}
	@Test
	public void turnLeftW()
	{
		Rover r = new Rover(1, 2, Direction.W);
		r.turnLeft();
		assertEquals(r.getD(), Direction.S);
		assertEquals(r.getCurrentX(), 1);
		assertEquals(r.getCurrentY(), 2);
	}
	@Test
	public void turnLeftS()
	{
		Rover r = new Rover(1, 2, Direction.S);
		r.turnLeft();
		assertEquals(r.getD(), Direction.E);
		assertEquals(r.getCurrentX(), 1);
		assertEquals(r.getCurrentY(), 2);
	}
	
	@Test
	public void turnRightN()
	{
		Rover r = new Rover(1, 2, Direction.N);
		r.turnRight();
		assertEquals(r.getD(), Direction.E);
		assertEquals(r.getCurrentX(), 1);
		assertEquals(r.getCurrentY(), 2);
	}
	@Test
	public void turnRightE()
	{
		Rover r = new Rover(1, 2, Direction.E);
		r.turnRight();
		assertEquals(r.getD(), Direction.S);
		assertEquals(r.getCurrentX(), 1);
		assertEquals(r.getCurrentY(), 2);
	}
	@Test
	public void turnRightW()
	{
		Rover r = new Rover(1, 2, Direction.W);
		r.turnRight();
		assertEquals(r.getD(), Direction.N);
		assertEquals(r.getCurrentX(), 1);
		assertEquals(r.getCurrentY(), 2);
	}
	@Test
	public void turnRightS()
	{
		Rover r = new Rover(1, 2, Direction.S);
		r.turnRight();
		assertEquals(r.getD(), Direction.W);
		assertEquals(r.getCurrentX(), 1);
		assertEquals(r.getCurrentY(), 2);
	}
	
	@Test
	public void turnMoveN()
	{
		Rover r = new Rover(1, 2, Direction.N);
		r.move();
		assertEquals(r.getCurrentX(), 1);
		assertEquals(r.getD(), Direction.N);
		assertEquals(r.getCurrentY(), 3);
	}
	@Test
	public void turnMoveE()
	{
		Rover r = new Rover(1, 2, Direction.E);
		r.move();
		assertEquals(r.getCurrentY(),2);
		assertEquals(r.getD(),  Direction.E);
		assertEquals(r.getCurrentX(),2);
	}
	
	@Test
	public void turnMoveW()
	{
		Rover r = new Rover(1, 2, Direction.W);
		r.move();
		assertEquals(r.getCurrentX(),0);
		assertEquals(r.getD(), Direction.W);
		assertEquals(r.getCurrentY(), 2);
	}
	@Test
	public void turnMoveS()
	{
		Rover r = new Rover(1, 2, Direction.S);
		r.move();
		assertEquals(r.getCurrentY(),1);
		assertEquals(r.getD(), Direction.S);
		assertEquals(r.getCurrentX(),1);
	}
}
