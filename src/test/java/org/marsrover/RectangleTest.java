package org.marsrover;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class RectangleTest {
	@Test
	public void validateValidTest()
	{
		Rover r = new Rover(1, 5, Direction.N);
		Rectangle rect= new Rectangle(5, 5);
		boolean valid = rect.validate(r);
		assertTrue(valid);
	}


	@Test
	public void validateXDirLess()
	{
		Rover r = new Rover(-11, 5, Direction.N);
		Rectangle rect= new Rectangle(5, 5);
		boolean valid = rect.validate(r);
		assertFalse(valid);
	}
	
	@Test
	public void validateYDirLess()
	{
		Rover r = new Rover(1, -55, Direction.N);
		Rectangle rect= new Rectangle(5, 5);
		boolean valid = rect.validate(r);
		assertFalse(valid);
	}
	
	@Test
	public void validateXMore()
	{
		Rover r = new Rover(6, 5, Direction.N);
		Rectangle rect= new Rectangle(5, 5);
		boolean valid = rect.validate(r);
		assertFalse(valid);
	}
	
	@Test
	public void validateYMore()
	{
		Rover r = new Rover(5, 6, Direction.N);
		Rectangle rect= new Rectangle(5, 5);
		boolean valid = rect.validate(r);
		assertFalse(valid);
	}
}

