package org.marsrover;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MainTest {

	@Test
	public void decideMoveLTest()
	{
		Main m = new Main();
		Rover r = new Rover(1, 2, Direction.N);
		m.decideMove(r, 'L');
		assertEquals(r.getD(),Direction.W);
		assertEquals(r.getCurrentX(),1);
		assertEquals(r.getCurrentY(),2);
	}
	@Test
	public void decideMoveRTest()
	{
		Main m = new Main();
		Rover r = new Rover(1, 2, Direction.N);
		m.decideMove(r, 'R');
		assertEquals(r.getD(),Direction.E);
		assertEquals(r.getCurrentX(),1);
		assertEquals(r.getCurrentY(),2);
	}
	@Test
	public void decideMoveMTest()
	{
		Main m = new Main();
		Rover r = new Rover(1, 2, Direction.N);
		m.decideMove(r, 'M');
		assertEquals(r.getD(),Direction.N);
		assertEquals(r.getCurrentX(),1);
		assertEquals(r.getCurrentY(),3);
	}
	@Test
	public void getDirectionNTest()
	{
		Main m = new Main();
		assertEquals(m.getDirection("N"),Direction.N);
	}
	@Test
	public void getDirectionETest()
	{
		Main m = new Main();
		assertEquals(m.getDirection("E"),Direction.E);
	}
	@Test
	public void getDirectionWTest()
	{
		Main m = new Main();
		assertEquals(m.getDirection("W"),Direction.W);
	}
	@Test
	public void getDirectionSTest()
	{
		Main m = new Main();
		assertEquals(m.getDirection("S"),Direction.S);
	}
}
