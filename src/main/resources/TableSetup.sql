


CREATE TABLE Employee (
    ID int NOT NULL PRIMARY KEY,
    LName varchar(255) NOT NULL,
    FName varchar(255) NOT NULL,
    City varchar(255) NOT NULL,
    State varchar(255) ,
    Mgr int
);
        
INSERT INTO Employee (ID,LName,FName,City,State,Mgr) VALUES (1, 'Davolio', 'Nancy', 'Seattle', 'WA', 2 );
INSERT INTO Employee (ID,LName,FName,City,State) VALUES (2, 'Fuller', 'Andrew', 'Tacoma', 'WA' );
INSERT INTO Employee (ID,LName,FName,City,State,Mgr) VALUES (3, 'Leverling', 'Janet', 'Kirkland', 'WA', 2 );
INSERT INTO Employee (ID,LName,FName,City,State,Mgr) VALUES (4, 'Peacock', 'Margaret', 'Redmond', 'WA', 2 );
INSERT INTO Employee (ID,LName,FName,City,Mgr) VALUES (5, 'Buchanan', 'Steven', 'London',  2 );
INSERT INTO Employee (ID,LName,FName,City,Mgr) VALUES (6, 'Suyama', 'Michael', 'London', 5 );
INSERT INTO Employee (ID,LName,FName,City,State,Mgr) VALUES (7, 'King', 'Robert', 'London', 'WA', 5 );
INSERT INTO Employee (ID,LName,FName,City,State,Mgr) VALUES (8, 'Callahan', 'Laura', 'Seattle', 'WA', 2 );
INSERT INTO Employee (ID,LName,FName,City,Mgr) VALUES (9, 'Dodsworth', 'Anne', 'London', 5 );

CREATE TABLE State (    
    Abbrev varchar(255) NOT NULL PRIMARY KEY,
    Name varchar(255) NOT NULL
);

INSERT INTO State VALUES('WA' , 'Washington');
INSERT INTO State VALUES('CA', 'California');
INSERT INTO State VALUES('NY', 'New York');
INSERT INTO State VALUES('AZ', 'Arizona' );