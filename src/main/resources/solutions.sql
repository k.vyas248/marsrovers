
--1:
SELECT "FName"
FROM public."Employee"
WHERE "LName" LIKE 'D%';
	
--2	
SELECT DISTINCT "City"
FROM public."Employee";

--3
Select (e.fname || " " || e.lname) as Employee ,(mgr.fname || " " || mgr.lname) as Mgr
FROM Employee as e Join Employee as mgr
On e.mgr = mgr.id;

--4
Select (mgr.fname || " " || mgr.lname) as Mgr FROM Employee as mgr
where mgr.id in (
SELECT mgr from Employee 
group by(mgr)
having count(fname)>3)

--5 
SELECT fname, lname, 
CASE 
WHEN state IS NULL
Then 'NOT KNOWN'
ELSE state 
END AS state
,
CASE 
WHEN Name IS NULL
Then 'NOT KNOWN'
ELSE Name 
END AS StateName
FROM Employee LEFT JOIN State
On Employee.state = State.Abbrev

	
	
