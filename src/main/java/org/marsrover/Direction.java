package org.marsrover;

/**
 * Enumeration Direction
 * @author Kruti
 *
 */
public enum Direction {
	N,E,S,W

}
