package org.marsrover;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
/**
 * Main Class 
 * @author Kruti
 *
 */
public class Main {

	/**
	 * Main method
	 * @param args [] array of Strings
	 */
	public static void main(String[] args)  {
		try {
		Main m = new Main();
		System.out.println("Enter the path of the file");
		BufferedReader s = new BufferedReader(new InputStreamReader(System.in));
		String path = s.readLine().trim();
		BufferedReader bf = new BufferedReader(new FileReader(path));
		String line = bf.readLine().trim();
		String temp[] = line.split(" ");
		Rectangle r = new Rectangle(Integer.parseInt(temp[0]), Integer.parseInt(temp[0]));
		Rover rover;
		boolean valid ;
		while ((line = bf.readLine())!= null) {
			valid = true;
			if(valid)
			{
				temp = line.split(" ");
				rover = new Rover(Integer.parseInt(temp[0]),Integer.parseInt(temp[1]),m.getDirection(temp[2]));
				line = bf.readLine();
				if(r.validate(rover))
				{
					for(int i=0;i<line.length();i++)
					{
						m.decideMove(rover, line.charAt(i));
						valid = r.validate(rover);
						if(!valid)
						{
							break;
						}
							
					}
				}
				else {
					System.out.println("INVALID INPUT!!!!");
				}
				if(!valid)
				{
					System.out.println("INVALID INPUT!!!!");
				}
				else
				{
					System.out.println(rover.getCurrentX()+" "+rover.getCurrentY()+" "+rover.getD());
				}
			}
		}

		}catch(IOException e)
		{
			System.out.println("Invalid Input File.");
		}
	}

	/**
	 * This method decides the whether to trun or move the rover
	 * @param rover the Rover
	 * @param move the char
	 */
	public void decideMove(Rover rover, char move)
	{
		switch(move)
		{
		case 'L':
			rover.turnLeft();
			break;
		case 'R':
			rover.turnRight();
			break;
		case 'M': 
			rover.move();
			break;
		}
	}
	
	/**
	 * This method converts the char into Direction
	 * @param d char
	 * @return Direction
	 */
	public Direction getDirection(String d) {
		Direction direction;
		switch (d) {

		case "N":
			return (Direction.N);

		case "S":
			return (Direction.S);

		case "E":
			return (Direction.E);

		case "W":
			return (Direction.W);

		default:
			return null;
		}

	}

}
