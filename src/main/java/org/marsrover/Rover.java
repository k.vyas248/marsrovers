package org.marsrover;
/**
 * Class Rover
 * @author Kruti
 *
 */
public class Rover {
	private int currentX;
	private int currentY;
	private Direction d;
	
	/**
	 * Constructor
	 * @param currentX int
	 * @param currentY int
	 * @param d Direction
	 */
	public Rover(int currentX ,int currentY, Direction d) {
		this.currentX = currentX;
		this.currentY = currentY;
		this.d = d;
	}
	public int getCurrentX() {
		return currentX;
	}

	public void setCurrentX(int currentX) {
		this.currentX = currentX;
	}

	public int getCurrentY() {
		return currentY;
	}

	public void setCurrentY(int currentY) {
		this.currentY = currentY;
	}

	public Direction getD() {
		return d;
	}

	public void setD(Direction d) {
		this.d = d;
	}

	/**
	 * Turn left move of rover
	 */
	public void turnLeft() {
		switch (this.d) {
		case N: {
			this.d = Direction.W;
			break;
		}
		case E: {
			this.d = Direction.N;
			break;
		}
		case S: {
			this.d = Direction.E;
			break;
		}
		case W: {
			this.d = Direction.S;
			break;
		}
		}
	}

	/**
	 * Turn right the move
	 */
	public void turnRight() {
		switch (this.d) {
		case N: {
			this.d = Direction.E;
			break;
		}
		case E: {
			this.d = Direction.S;
			break;
		}
		case S: {
			this.d = Direction.W;
			break;
		}
		case W: {
			this.d = Direction.N;
			break;
		}
		}
	}

	/**
	 * Move the rover
	 */
	public void move() {
		switch (this.d) {
		case N: {
			this.currentY+=1;
			break;
		}
		case E: {
			this.currentX+=1;
			break;
		}
		case S: {
			this.currentY-=1;
			break;
		}
		case W: {
			this.currentX-=1;
			break;
		}
		}
	}
	
}
