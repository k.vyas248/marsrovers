package org.marsrover;

/**
 * Rectangle class 
 * @author Kruti
 *
 */
public class Rectangle {

	private int height;
	private int width;
	/**
	 * Constructor
	 * @param width the int
	 * @param height the int
	 */
	Rectangle(int width, int height)
	{
		this.height=height;
		this.width=width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWidth() {
		return width;
	}
	public void setWeidth(int width) {
		this.width = width;
	}
	
	/**
	 * This method validates the move of rover
	 * @param r Rover
	 * @return boolean 
	 */
	public boolean validate(Rover r)
	{
		boolean valid=true;
		if(r.getCurrentX()<0 || r.getCurrentX()>this.getWidth())
		{
			valid=false;
		}
		if(r.getCurrentY()<0 || r.getCurrentY()>this.getHeight())
		{
			valid=false;
		}
		return valid;
	}
	
}
